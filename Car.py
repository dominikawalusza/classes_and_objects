class Car:

    #konstruktor, służy do inicjalizacji obiektu
    def __init__(self, model, brand, year, color, mileage): 
        self.model = model
        self.brand = brand
        self.year = year
        self.color = color
        self.mileage = mileage
        self.speed = 0

    #rzutowanie do stringa (__str__ metoda zarezerwowana)
    def __str__(self):
        return(f"Model: {self.model}\n" 
        f"Marka: {self.brand}\n"
        f"Rok produkcji: {self.year}\n" 
        f"Kolor: {self.color}\n"
        f"Przebieg: {self.mileage}\n")

    def start(self):
        if self.speed == 0:
            print("START!")
            self.speed = 10
        else:
            print("Samochód już jedzie.")
    
    def speed_up(self, some_speed):
        if self.speed > 0:
            self.speed += some_speed
        else:
            print("Nie można przyspieszyć.")

    def stop(self):
        if self.speed == 0:
            print("Samochód nie porusza się.")
        else:
            self.speed = 0
            print("Samochód zatrzymał się.")

    def slow_down(self, some_speed):
        if self.speed == 0:
            print("Samochód nie porusza się.")
            return

        if some_speed >= self.speed:
            self.speed = 0
            print("Samochód zatrzymał się.")
            return

        if some_speed > 0:
            self.speed -= some_speed
            print(f"Samochód zwolnił o {some_speed} km/h")
        else:
            print(f"Nie można zwolnić o {some_speed} km/h")
    
    def beep(self):
        print("BEEP! BEEP!")