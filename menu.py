from Car import Car

def display_menu():
    brand = "VW"
    model = "Polo"
    year = 1998
    color = "pink"
    mileage = 170000

    my_car = Car(model, brand, year, color, mileage)

    
    while (True):
        print_menu()
        choice = int(input())

        if choice == 1:
            my_car.start()
        elif choice == 2:
            some_speed = int(input("O ile chcesz przyspieszyć? "))
            my_car.speed_up(some_speed)
        elif choice == 3:
            some_speed = int(input("O ile chcesz zwolnić? "))
            my_car.slow_down(some_speed)
        elif choice == 4:
            my_car.stop()
        elif choice ==5:
            my_car.beep()
        elif choice == 6:
            print(f"Prędkość na blacie to: {my_car.speed}")
        elif choice ==7:
            break

def print_menu():
    print("""
1. Wystartuj
2. Przyspiesz
3. Spowolnij
4. Zatrzymaj się
5. Zatrąb
6. Sprawdź prędkość
7. Zakończ.
        """)